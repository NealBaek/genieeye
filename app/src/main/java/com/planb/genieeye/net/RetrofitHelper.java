package com.planb.genieeye.net;

import android.content.Context;

import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by admin on 2017. 10. 20..
 */

public class RetrofitHelper {


    private Retrofit retrofit = null;
    private static RetrofitHelper self = null;
    private GenieEyeAPIInterface genieEyeAPIInterface = null;

    private static final String MAIN_URL = "http://222.106.61.66:5008";

    private SSLSocketFactory sslSocketFactory  =null;
    private HostnameVerifier hostnameVerifier  =null;
    private X509TrustManager x509TrustManager  =null;

    private RetrofitHelper(){}

    public void launch(Context context){

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build();


        GsonConverterFactory converterFactory = GsonConverterFactory.create();


        retrofit = new Retrofit.Builder()
                .baseUrl( MAIN_URL )
                .addConverterFactory (converterFactory)
                .client              (okHttpClient)
                .build();


    }

    public static RetrofitHelper getInstance(){

        if( self == null ){

            self = new RetrofitHelper();

        }

        return self;
    }

    public GenieEyeAPIInterface getProfileAPI(){

        if( genieEyeAPIInterface == null ){

            genieEyeAPIInterface = retrofit.create( GenieEyeAPIInterface.class );

        }

        return genieEyeAPIInterface;
    }

}
