package com.planb.genieeye.net;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;


/**
 * Created by admin on 2017. 10. 20..
 */

public interface GenieEyeAPIInterface {

    /** URL :  */

    @Multipart
    @POST("/api/v1/analysis/detect/url/android.jpg")
    Call<ResGenieEyeVo> callGenieEyeAPI( @Part MultipartBody.Part img );


}
