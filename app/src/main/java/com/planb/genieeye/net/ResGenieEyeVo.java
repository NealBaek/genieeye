package com.planb.genieeye.net;

import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by admin on 2018. 1. 10..
 */
@Getter @Setter @ToString
public class ResGenieEyeVo {

    Result result;
    String status;


    @Getter @Setter @ToString
    public class Result{
        public String prob;
        public String url;
    }


//    {'result':
//        {'prob': '["0.9977", "0.0010", "0.0002", "0.0009", "0.0001", "0.0000"]', 
//            'url': 'http://175.209.243.53:5008/static/data/dst/170921-162458_detect_1.jpg'}, 
//        'status': 'ok'}
}
