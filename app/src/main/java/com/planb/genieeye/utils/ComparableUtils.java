package com.planb.genieeye.utils;

import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * Created by admin on 2018. 1. 15..
 */

public class ComparableUtils {



    public static Float selectBiggest( float... floatArray ) throws Exception{

        Arrays.sort(floatArray);

        L.getInstance().log(ComparableUtils.class,"[ ComparableUtils ] selectBiggest : " + floatArray[floatArray.length - 1]);

        return floatArray[floatArray.length - 1];
    }


}
