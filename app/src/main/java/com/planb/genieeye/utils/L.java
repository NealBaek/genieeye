package com.planb.genieeye.utils;

import android.util.Log;

import java.lang.ref.WeakReference;


public class L {
    private static L                instance    =null;
    private static final String     TAG         ="ABC";
    private static final String     ARROW       =" => ";

    public static L getInstance(){
        if( instance == null)
            instance = new L();

        return instance;
    }

    public void log(Class c, String msg){
        WeakReference<Class>    cName  =null;
        WeakReference<String>   msgRef =null;
        int dotPoint;
        int strLength;


        cName     = new WeakReference<Class>(c);
        msgRef       = new WeakReference<String>(msg);

        dotPoint  = cName.get().toString().lastIndexOf(".") + 1;
        strLength = cName.get().toString().length();

        Log     .i(TAG, new String(cName.get().toString().subSequence(dotPoint, strLength) + ARROW + msgRef.get()));

        cName   .clear();
        msgRef  .clear();
    }

    public void logArray(Class c, Object...objs){

        WeakReference<Class>        cName;
        WeakReference<Object[]>     objsClone;


        cName       = new WeakReference<Class>(c);
        objsClone   = new WeakReference<Object[]>(objs);

        int dotPoint    = cName.get().toString().lastIndexOf(".") + 1;
        int strLength   = cName.get().toString().length();

        Object[] objArray       =   objsClone   .get();
        int      length         =   objArray    .length;


        for(int i = 0; i < length; ++ i){
            Log.i(TAG, new String(cName.get().toString().subSequence(dotPoint, strLength) + ARROW + objArray[i].toString()));
        }


        cName       .clear();
        objsClone   .clear();
    }
}
