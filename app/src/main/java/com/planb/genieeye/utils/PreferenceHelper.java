package com.planb.genieeye.utils;

import android.content.Context;
import android.content.SharedPreferences;


public class PreferenceHelper {

    private static PreferenceHelper instance =null;
    private Context context = null;
    private PreferenceHelper(){}

    public static PreferenceHelper getInstance(){

        if( instance == null ){
            instance = new PreferenceHelper();
        }
        return instance;
    }

    public void setContext(Context context){
        this.context = context;
        sp = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        editor = sp.edit();
    }


    private SharedPreferences        sp     = null;
    private SharedPreferences.Editor editor = null;



    private final String VALUE_NONE        = "";
    private final String PREF_FILE_NAME    = "BUDDY_COIN";
    private final String KEY_COOKIE        = "COOKIE";







    /** OTP 6자리 비밀번호 */
    private final String KEY_OTP_NUMBER    = "OTP_NUMBER";
    public static final String NO_SUCH_PASSWORD = "null";

    public void setOTPNumber(String otpNumber){

        editor.putString( KEY_OTP_NUMBER, otpNumber );
        editor.commit();

    }
    public String getOTPNumber(){

        return sp.getString(KEY_OTP_NUMBER, NO_SUCH_PASSWORD);
    }









    /** 보안 토큰 */

    private final String KEY_AUTH_TOKEN    = "AUTH_TOKEN";

    public void setAuthToken(String authToken){

        editor.putString( KEY_AUTH_TOKEN, authToken );
        editor.commit();

    }
    public String getAuthToken(){

        return sp.getString( KEY_AUTH_TOKEN , VALUE_NONE);
    }








//    /** 유저 정보  */
//
//    private final String KEY_USER_INFO     = "USER_INFO";
//
//    public void setUserInfo(UserInfoVo info) {
//        Gson gson = new Gson();
//        editor.putString(KEY_USER_INFO, gson.toJson(info)).apply();
//        editor.commit();
//    }
//    public UserInfoVo getUserInfo() {
//        Gson gson = new Gson();
//        return gson.fromJson( sp.getString(KEY_USER_INFO, null), UserInfoVo.class);
//    }
//
//
//
//
//
//
//
//    /** Balance 정보  */
//
//    private final String KEY_USER_BALANCE  = "USER_BALANCE";
//
//    public void setUserBalance(ResBalanceVo info) {
//        Gson gson = new Gson();
//        editor.putString(KEY_USER_BALANCE, gson.toJson(info)).apply();
//        editor.commit();
//    }
//    public ResBalanceVo getUserBalance() {
//        Gson gson = new Gson();
//        return gson.fromJson( sp.getString(KEY_USER_BALANCE, null), ResBalanceVo.class);
//    }
//
//
//
//
//
//
//    /** 자동로그인 */
//
//    // 자동 로그인 플래그
//    private final String KEY_AUTO_LOGIN    = "AUTO_LOGIN";
//
//    public void setAutoLoginEnable( boolean isAutoLoginEnable ){
//        editor.putBoolean(KEY_AUTO_LOGIN, isAutoLoginEnable).apply();
//        editor.commit();
//    }
//    public boolean isAutoLoginEnable(){
//        return sp.getBoolean(KEY_AUTO_LOGIN, false);
//    }
//
//    // 자동 로그인 유저 정보
//    private final String KEY_AUTO_LOGIN_INFO    = "AUTO_LOGIN_INFO";
//
//    public void setAutoLoginInfo( AutoLoginModel autoLoginModel ){
//        Gson gson = new Gson();
//        editor.putString(KEY_AUTO_LOGIN_INFO, gson.toJson(autoLoginModel)).apply();
//        editor.commit();
//    }
//    public AutoLoginModel getAutoLoginInfo(){
//        Gson gson = new Gson();
//        return gson.fromJson( sp.getString(KEY_AUTO_LOGIN_INFO, null), AutoLoginModel.class );
//    }
//
//
//
//
//
//
//    /** 앱 설치 후 첫 실행 */
//
//    private final String KEY_FIRST_LAUNCH  = "FIRST_LAUNCH";
//
//    public void setFirstLaunch( boolean isFirstLaunch ){
//        editor.putBoolean(KEY_FIRST_LAUNCH, isFirstLaunch).apply();
//        editor.commit();
//    }
//    public boolean isFirstLaunch(){
//
//        return sp.getBoolean(KEY_FIRST_LAUNCH, false);
//    }

}
