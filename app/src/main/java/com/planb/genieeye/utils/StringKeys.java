package com.planb.genieeye.utils;

import com.planb.genieeye.R;

/**
 * Created by admin on 2018. 1. 10..
 */

public class StringKeys {

    public static final String WATCH_POSITION = "WATCH_POSITION";

    public static final String[] nameArray   = { "Rolex DateJust", "Rolex Explorer", "Omega Sea Master", "", "" };
    public static final String[] numberArray = { "126334", "214270", "21230412003001", "", "" };
    public static final int[] imageArray     = { R.drawable.watch_rolex_date_just, R.drawable.watch_rolex_explorer, R.drawable.watch_omega_seamaster };

    public static final String WATCH_IMG_PATH = "WATCH_IMG";
    public static final String WATCH_PROB     = "WATCH_PROB";

}
