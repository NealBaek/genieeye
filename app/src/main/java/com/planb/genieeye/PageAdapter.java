package com.planb.genieeye;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.planb.genieeye.utils.L;

/**
 * Created by admin on 2018. 1. 12..
 */

public class PageAdapter extends FragmentPagerAdapter {

    public static final int FRAGMENT_CNT = 6;

    private LoadingStep1Fragment step1Fragment =null;
    private LoadingStep2Fragment step2Fragment =null;
    private LoadingStep3Fragment step3Fragment =null;
    private LoadingStep4Fragment step4Fragment =null;
    private LoadingStep5Fragment step5Fragment =null;
    private LoadingStep6Fragment step6Fragment =null;

    private float result =0;


    public PageAdapter(FragmentManager fm, float result) {

        super(fm);

        this.result = result;
    }

    @Override
    public Fragment getItem(int position) {

        L.getInstance().log(this.getClass(),"[ Loading ] position : " + position);

        switch (position){
            case 0 :

                if(step1Fragment == null){
                    step1Fragment = LoadingStep1Fragment.newInstance();
                }

                return step1Fragment;

            case 1 :

                if(step2Fragment == null){
                    step2Fragment = LoadingStep2Fragment.newInstance();
                }

                return step2Fragment;

            case 2 :

                if(step3Fragment == null){
                    step3Fragment = LoadingStep3Fragment.newInstance();
                }

                return step3Fragment;

            case 3 :

                if(step4Fragment == null){
                    step4Fragment = LoadingStep4Fragment.newInstance();
                }

                return step4Fragment;

            case 4 :

                if(step5Fragment == null){
                    step5Fragment = LoadingStep5Fragment.newInstance();
                }

                return step5Fragment;

            case 5 :

                if(step6Fragment == null){
                    step6Fragment = LoadingStep6Fragment.newInstance(result);
                }

                return step6Fragment;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return FRAGMENT_CNT;
    }


//    @Override
//    public Object instantiateItem(ViewGroup container, int position) {
//
//
//        switch (position){
//            case 0 :
//
//                if(step1Fragment == null){
//                    step1Fragment = LoadingStep1Fragment.newInstance();
//                }
//
//                return step1Fragment;
//
//            case 1 :
//
//                if(step2Fragment == null){
//                    step2Fragment = LoadingStep2Fragment.newInstance();
//                }
//
//                return step2Fragment;
//
//            case 2 :
//
//                if(step3Fragment == null){
//                    step3Fragment = LoadingStep3Fragment.newInstance();
//                }
//
//                return step3Fragment;
//
//            case 3 :
//
//                if(step4Fragment == null){
//                    step4Fragment = LoadingStep4Fragment.newInstance();
//                }
//
//                return step4Fragment;
//
//            case 4 :
//
//                if(step5Fragment == null){
//                    step5Fragment = LoadingStep5Fragment.newInstance();
//                }
//
//                return step5Fragment;
//
//
//            case 5 :
//
//                if(step6Fragment == null){
//                    step6Fragment = LoadingStep6Fragment.newInstance(result);
//                }
//
//                return step6Fragment;
//
//            default :
//                return super.instantiateItem(container, position);
//
//        }
//
//    }
}
