package com.planb.genieeye;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.planb.genieeye.base.BaseFragmentContracts;
import com.planb.genieeye.databinding.FragmentLoadingStep5Binding;

public class LoadingStep5Fragment extends Fragment implements BaseFragmentContracts.BaseView {

    private FragmentLoadingStep5Binding step5Binding = null;

    public LoadingStep5Fragment() {
        // Required empty public constructor
    }


    public static LoadingStep5Fragment newInstance() {
        LoadingStep5Fragment fragment = new LoadingStep5Fragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
//        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        step5Binding = DataBindingUtil.inflate(inflater, R.layout.fragment_loading_step5, container, false);
        step5Binding.setStep5Fragment(this);

        return step5Binding.getRoot();

    }

    @Override
    public void initAllViews() {

    }

}
