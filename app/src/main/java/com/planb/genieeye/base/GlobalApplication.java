package com.planb.genieeye.base;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.planb.genieeye.net.RetrofitHelper;

import java.lang.ref.WeakReference;


/**
 * Created by admin on 2017. 11. 15..
 */

public class GlobalApplication extends Application {

    private static volatile GlobalApplication instance   =null;


    public static GlobalApplication getGApplicationContext(){
        if(instance == null)
            throw new IllegalStateException("CustomException GlobalApplication 를 Manifests에 등록하십시오");

        return new WeakReference<GlobalApplication>( instance ).get();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        instance    =this;

//        KakaoSDKAdapter sdkAdapter  =new KakaoSDKAdapter();
//        KakaoSDK.init(sdkAdapter);


//        FacebookSdk.sdkInitialize( conRef.get() );
//        PreferenceHelper.getInstance().setContext( getGApplicationContext() );
        RetrofitHelper.getInstance().launch( getGApplicationContext() );
//        ColorUtil.getInstance().setContext( getGApplicationContext() );

    }
    @Override
    public void onTerminate() {
        super.onTerminate();
        instance    =null;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);

    }
}
