package com.planb.genieeye.base;

/**
 * Created by admin on 2017. 12. 13..
 */

public interface BaseFragmentContracts {

    interface BaseView{

        void initAllViews();

    }

    interface BasePresenter{

        void init();

        void clearRef();

    }

}
