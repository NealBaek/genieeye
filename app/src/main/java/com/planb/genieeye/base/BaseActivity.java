package com.planb.genieeye.base;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;


import com.planb.genieeye.R;

import java.lang.ref.WeakReference;

import cn.pedant.SweetAlert.SweetAlertDialog;


/**
 * Created by admin on 2017. 11. 16..
 */

public class BaseActivity extends AppCompatActivity {

    private SweetAlertDialog pDialog = null;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    /** 상태바 색 바꾸기 */
    protected void colorStatusBar(int COLOR ){

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){  // LOLLIPOP = 21
            WeakReference<Window> refWin = new WeakReference<Window>(getWindow());

            refWin.get().clearFlags        (WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            refWin.get().addFlags          (WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            refWin.get().setStatusBarColor (COLOR);

            refWin.clear();
        }
    }



    /** 프로그래스 다이얼로그 */
    public void showProgress(String title){

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if( pDialog == null ){

                    pDialog = new SweetAlertDialog(BaseActivity.this, SweetAlertDialog.PROGRESS_TYPE);

                }

                if( !pDialog.isShowing() ){
                    pDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.mainColor));
                    pDialog.setTitleText(title);
                    pDialog.setCancelable(false);
                    pDialog.show();
                }
            }
        });

    }
    public void dismissProgress(){

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if( pDialog == null){
                    return;
                }

                if( pDialog.isShowing()){
                    pDialog.dismiss();
                }

            }
        });

    }


    /** 소프트 키보드 */
    public void hideKeyBoard(){

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                WeakReference<View> refView = new WeakReference<View>(getCurrentFocus());

                if (refView.get() != null) {
                    WeakReference<InputMethodManager> refIMM = new WeakReference<InputMethodManager>((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE));

                    refIMM.get()
                            .hideSoftInputFromWindow(refView.get().getWindowToken(), 0);
                    refIMM.clear();
                }

                refView.clear();
            }
        });

    }







    @Override
    public void onBackPressed() {
        hideKeyBoard();
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();

        dismissProgress();

        pDialog = null;
    }


}
