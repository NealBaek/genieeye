package com.planb.genieeye.base;

/**
 * Created by admin on 2017. 10. 17..
 */

public interface BaseContracts {


    interface BaseView{

        void setBinding();

        void initAllViews();

    }

    interface BasePresenter{

        void init();

        void clearRef();

    }

}
