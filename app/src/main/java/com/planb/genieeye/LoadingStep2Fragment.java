package com.planb.genieeye;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.planb.genieeye.base.BaseFragmentContracts;
import com.planb.genieeye.databinding.FragmentLoadingStep2Binding;

public class LoadingStep2Fragment extends Fragment implements BaseFragmentContracts.BaseView{

    private FragmentLoadingStep2Binding step2Binding = null;

    public LoadingStep2Fragment() {
        // Required empty public constructor
    }


    public static LoadingStep2Fragment newInstance() {
        LoadingStep2Fragment fragment = new LoadingStep2Fragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
//        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        step2Binding = DataBindingUtil.inflate(inflater, R.layout.fragment_loading_step2, container, false);
        step2Binding.setStep2Fragment(this);

        return step2Binding.getRoot();

    }

    @Override
    public void initAllViews() {

    }
}
