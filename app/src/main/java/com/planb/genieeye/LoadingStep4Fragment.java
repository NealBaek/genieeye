package com.planb.genieeye;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.planb.genieeye.base.BaseFragmentContracts;
import com.planb.genieeye.databinding.FragmentLoadingStep4Binding;

public class LoadingStep4Fragment extends Fragment implements BaseFragmentContracts.BaseView {

    private FragmentLoadingStep4Binding step4Binding = null;

    public LoadingStep4Fragment() {
        // Required empty public constructor
    }


    public static LoadingStep4Fragment newInstance() {
        LoadingStep4Fragment fragment = new LoadingStep4Fragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
//        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        step4Binding = DataBindingUtil.inflate(inflater, R.layout.fragment_loading_step4, container, false);
        step4Binding.setStep4Fragment(this);

        return step4Binding.getRoot();

    }

    @Override
    public void initAllViews() {

    }
}
