package com.planb.genieeye;

import android.animation.Animator;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.CycleInterpolator;
import android.widget.ImageView;

import com.planb.genieeye.base.BaseActivity;
import com.planb.genieeye.base.BaseContracts;
import com.planb.genieeye.databinding.ActivityLoadingStartBinding;
import com.planb.genieeye.utils.L;
import com.planb.genieeye.utils.StringKeys;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class LoadingStartActivity extends BaseActivity implements BaseContracts.BaseView {

    private ActivityLoadingStartBinding loadingStartBinding = null;

    private Animation razorUpAnim    = null;
    private Animation razorDownAnim  = null;
    private Animation razorLeftAnim  = null;
    private Animation razorRightAnim = null;

    private AnimationSet fadingAnimationSet = null;

    private PageAdapter pageAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setBinding();
        initAllViews();
    }


    @Override
    public void setBinding() {

        loadingStartBinding = DataBindingUtil.setContentView(this, R.layout.activity_loading_start);
        loadingStartBinding.setLoadingStartActivity(this);

    }

    @Override
    public void initAllViews() {

        overridePendingTransition(R.anim.activity_fade_in, R.anim.activity_fade_out);


        setUpWatchInfo();
        setUpViewPager();
        setUpAnimations();



        handler.sendEmptyMessage(0);

    }

    private void setUpWatchInfo(){

        String watchImgPath   = getIntent().getStringExtra(StringKeys.WATCH_IMG_PATH);
        int    watchPos  = getIntent().getIntExtra(StringKeys.WATCH_POSITION, 0);
        String watchName = StringKeys.nameArray[watchPos];
        String watchNum  = StringKeys.numberArray[watchPos];


        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loadingStartBinding.watchNameTv.setText( watchName );
                loadingStartBinding.watchProductNumberTv.setText( watchNum );
            }
        });

        loadingStartBinding.watchImg.post(new Runnable() {
            @Override
            public void run() {
                loadingStartBinding.watchImg.setRotation(90);
                loadingStartBinding.watchImg.setImageURI( Uri.parse( watchImgPath ) );
            }
        });

    }

    private void setUpViewPager(){

        float result   = getIntent().getFloatExtra(StringKeys.WATCH_PROB, 0);
        pageAdapter    = new PageAdapter(getSupportFragmentManager(), result);



        loadingStartBinding.viewPager.setOffscreenPageLimit(6);
        loadingStartBinding.viewPager.post(new Runnable() {
            @Override
            public void run() {
                loadingStartBinding.viewPager.setAdapter( pageAdapter );
            }
        });

        loadingStartBinding.viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if( position ==  pageAdapter.FRAGMENT_CNT - 1 ){

                    (( LoadingStep6Fragment )pageAdapter.getItem( position ) ).showPopUp();

                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void setUpAnimations(){

        setUpRazorTranslationAnimation();

    }

    private void setUpRazorTranslationAnimation(){
        razorUpAnim    = AnimationUtils.loadAnimation(this, R.anim.razor_up);
        razorDownAnim  = AnimationUtils.loadAnimation(this, R.anim.razor_down);
        razorLeftAnim  = AnimationUtils.loadAnimation(this, R.anim.razor_left);
        razorRightAnim = AnimationUtils.loadAnimation(this, R.anim.razor_right);

        razorUpAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                loadingStartBinding.razorUp.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                loadingStartBinding.razorUp.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        razorDownAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                loadingStartBinding.razorDown.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                loadingStartBinding.razorDown.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        razorLeftAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                loadingStartBinding.razorLeft.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                loadingStartBinding.razorLeft.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        razorRightAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                loadingStartBinding.razorRight.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                loadingStartBinding.razorRight.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }




    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            if(msg.what > 7){
                return;
            }

            showAnimation(msg.what);

            loadingStartBinding.viewPager.setCurrentItem(msg.what, true);
            handler.sendEmptyMessageDelayed(msg.what + 1, 4800);

        }
    };

    private void showAnimation( int pos ){

        switch (pos){
            case 0 :    showRazorAnimation();             break;
            case 1 :    showImageFadingAnimation();       break;
            case 2 :    showCircleTranslatingAnimation(); break;
            case 3 :    showCircleRotatingAnimation();    break;
            case 4 :    showImageShakingAnimation();      break;
            case 5 :    stopAnimations();                 break;
        }

    }

    private void showRazorAnimation(){

        loadingStartBinding.razorUp.startAnimation(razorUpAnim);
        loadingStartBinding.razorDown.startAnimation(razorDownAnim);
        loadingStartBinding.razorLeft.startAnimation(razorLeftAnim);
        loadingStartBinding.razorRight.startAnimation(razorRightAnim);

    }

    private void showImageFadingAnimation(){

        for( int i =0; i< 3; ++i){

            loadingStartBinding.watchImg.postDelayed(new Runnable() {
                @Override
                public void run() {
                    loadingStartBinding.watchImg.animate().alpha(0.3f).setDuration(1000).setListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            loadingStartBinding.watchImg.animate().alpha(1.0f).setDuration(1000).start();
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    }).start();
                }
            }, i * 2000);

        }



    }

    private void showCircleTranslatingAnimation(){

    }

    private void showCircleRotatingAnimation(){


    }

    private void showImageShakingAnimation(){

        for( int i =0; i< 5; ++i){

            loadingStartBinding.watchImg.postDelayed(new Runnable() {
                @Override
                public void run() {

                    loadingStartBinding.watchImg.animate()
                            .translationX(10).setDuration(200).setInterpolator(new CycleInterpolator(3)).start();
                }
            }, i * 1000);

        }

    }


    private void stopAnimations(){

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                loadingStartBinding.razorUp.setVisibility(View.INVISIBLE);
                loadingStartBinding.razorDown.setVisibility(View.INVISIBLE);
                loadingStartBinding.razorLeft.setVisibility(View.INVISIBLE);
                loadingStartBinding.razorRight.setVisibility(View.INVISIBLE);

                loadingStartBinding.razorUp.clearAnimation();
                loadingStartBinding.razorDown.clearAnimation();
                loadingStartBinding.razorLeft.clearAnimation();
                loadingStartBinding.razorRight.clearAnimation();

                razorUpAnim.setAnimationListener(null);
                razorDownAnim.setAnimationListener(null);
                razorLeftAnim.setAnimationListener(null);
                razorRightAnim.setAnimationListener(null);

            }
        });
    }

    public void onCancel(View v){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_fade_in, R.anim.activity_fade_out);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        handler.removeCallbacksAndMessages(null);
        handler = null;
    }
}
