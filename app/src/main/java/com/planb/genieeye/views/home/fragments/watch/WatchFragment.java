package com.planb.genieeye.views.home.fragments.watch;

import android.app.ActivityOptions;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.planb.genieeye.R;
import com.planb.genieeye.databinding.FragmentWatchBinding;
import com.planb.genieeye.utils.L;
import com.planb.genieeye.utils.StringKeys;
import com.planb.genieeye.views.home.HomeActivity;
import com.planb.genieeye.views.recognition.RecognitionActivity;
import com.yarolegovich.discretescrollview.DiscreteScrollView;

import java.util.ArrayList;

/**
 * Created by admin on 2018. 1. 9..
 */

public class WatchFragment extends Fragment implements WatchContract.View{

    private WatchPresenterImpl watchPresenter = null;
    private FragmentWatchBinding watchBinding = null;
    private WatchAdapter watchAdapter = null;


    public static WatchFragment createFor(String text) {
        WatchFragment fragment = new WatchFragment();
        Bundle args = new Bundle();
//        args.putString(EXTRA_TEXT, text);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        watchBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_watch, container, false);
        watchBinding.setWatchFragment(this);

        watchPresenter = new WatchPresenterImpl();
        watchPresenter.setFragment(this);
        watchPresenter.init();

        L.getInstance().log(this.getClass(),"[ onCreateView! ]");

        return watchBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();

        L.getInstance().log(this.getClass(),"[ onResume! ]");

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        L.getInstance().log(this.getClass(),"[ onAttach! ]");
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        L.getInstance().log(this.getClass(),"[ onViewCreated! ]");
    }

    @Override
    public void initAllViews() {
        setUpWatchSV();
    }

    private void setUpWatchSV(){


        watchBinding.watchImgSv.post(new Runnable() {
            @Override
            public void run() {

                watchAdapter = new WatchAdapter( generateFakeData() );
                watchBinding.watchImgSv.setAdapter( watchAdapter );
                watchBinding.watchImgSv.addOnItemChangedListener(new DiscreteScrollView.OnItemChangedListener<RecyclerView.ViewHolder>() {
                    @Override
                    public void onCurrentItemChanged(@Nullable RecyclerView.ViewHolder viewHolder, int cPos) {

                        onWatchChanged( cPos );

                    }
                });

            }
        });


        onWatchChanged(0);

    }

    private void onWatchChanged( int cPos ){
        watchBinding.getRoot().post(new Runnable() {
            @Override
            public void run() {
                watchBinding.watchNameTv.setText(StringKeys.nameArray[cPos]);
                watchBinding.watchProductNumberTv.setText(StringKeys.numberArray[cPos]);
                watchBinding.watchBtnText.setText( "내 " + StringKeys.nameArray[cPos] + " 테스트하기" );
            }
        });

    }



    private ArrayList<WatchModel> generateFakeData(){

        ArrayList<WatchModel> arrayList = new ArrayList<>();

        for( int i = 0; i < StringKeys.imageArray.length; ++i ){

            WatchModel watchModel = WatchModel.builder()
                                              .watchName( StringKeys.nameArray[i] )
                                              .watchModelNumber(StringKeys.numberArray[i] )
                                              .watchImg ( StringKeys.imageArray[i]  )
                                              .build();

            arrayList.add(watchModel);

        }

        return arrayList;

    }



    @Override
    public void startRecognitionActivity(View v) {

        Intent intent = new Intent(getActivity(), RecognitionActivity.class);
        intent.putExtra(StringKeys.WATCH_POSITION, watchBinding.watchImgSv.getCurrentItem());
        startActivity(intent);

    }
}
