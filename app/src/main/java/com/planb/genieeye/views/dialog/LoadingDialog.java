package com.planb.genieeye.views.dialog;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.planb.genieeye.LoadingStartActivity;
import com.planb.genieeye.R;
import com.planb.genieeye.utils.L;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

public class LoadingDialog extends ProgressDialog {

    private Context context;
    private AVLoadingIndicatorView indicatorView;

    private PieChart pieChart = null;

    private float prob;

    public LoadingDialog(Context context ,float prob){
        super(context);

        this.context = context;
        this.prob    = prob;

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawableResource(R.drawable.bg_dialog);
        setCanceledOnTouchOutside(false);

    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_loading);

        setUpPieChart();

        findViewById(R.id.header_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
    }

    private void setUpPieChart(){

        pieChart = findViewById(R.id.pie_chart);


        PieData pieData = parsePieData( 100 * prob );

        L.getInstance().log(this.getClass(),"[ onPictureTaken ] pieData  prob: " + prob);


        pieChart.setUsePercentValues(true);
        pieChart.getDescription().setEnabled(false);
        pieChart.setExtraOffsets(5, 10, 5, 5);

        pieChart.setDragDecelerationFrictionCoef(0.55f);

        pieChart.setTransparentCircleColor( context.getResources().getColor(R.color.mainColor) );
        pieChart.setTransparentCircleAlpha(110);
        pieChart.setTransparentCircleRadius(15f);


        // Holo = 중앙 원 크기
        pieChart.setDrawHoleEnabled(true);
        pieChart.setHoleColor( context.getResources().getColor(R.color.transParent) );
        pieChart.setHoleRadius( 34f );


//        int highestPercentage = ( int ) pieData.getDataSets().get(0).getEntryForIndex(0).getValue();


//        L.getInstance().log(this.getClass(),"[ onPictureTaken ] pieData  highestPercentage: " + highestPercentage);

        pieChart.setDrawCenterText( true );
        pieChart.setCenterText( (prob * 100) + "%" );
//        pieChart.setCenterTextTypeface( Typeface.createFromAsset( getAssets(), "fonts/BMJUA_ttf.ttf" ) );
//        pieChart.setCenterTextColor( Color.rgb(233, 118, 62) );
        pieChart.setCenterTextSize(22f);
        pieChart.setCenterTextColor( context.getResources().getColor(R.color.pointColor) );



        pieChart.setRotationAngle(0);
        pieChart.setRotationEnabled(false);





        // pieChart.setUnit(" €");
        // pieChart.setDrawUnitsInChart(true);

        // add a selection listener
//        pieChart.setOnChartValueSelectedListener(this);

        Legend l = pieChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(2f);
        l.setYOffset(2f);

        // entry label styling
        pieChart.setDrawEntryLabels(true);
        pieChart.setEntryLabelColor( context.getResources().getColor(R.color.mainColor) );
//        pieChart.setEntryLabelTypeface( Typeface.createFromAsset( getAssets(), "fonts/BMJUA_ttf.ttf" ) );
        pieChart.setEntryLabelTextSize(21.5f);
        pieChart.setDescription(null);

//        pieChart.invalidate();

        pieChart.setData(pieData);


        pieChart.setHighlightPerTapEnabled(true);

        pieChart.post(new Runnable() {
            @Override
            public void run() {

                int size = pieData.getDataSets().size();

                Highlight[] highlights = new Highlight[size];

                for( int z = 0; z < size; ++ z){

                    Highlight h = new Highlight( 0, z, 0); // dataset index for piechart is always 0

                    highlights[z] = h;
                }

                pieChart.highlightValues( highlights );

                pieChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);

            }
        });
    }

    int[] colorArray = {
            Color.rgb(138, 185, 94), Color.rgb(234, 234, 234), Color.rgb(34, 36, 46)};

    private PieData parsePieData( float prob ){

        ArrayList<PieEntry> pieEntryList = new ArrayList<>();

        pieEntryList.add( new PieEntry( prob , "진품") );
        pieEntryList.add( new PieEntry( 100 - prob , "가품") );

        // PieData

        PieDataSet pieDataSet = new PieDataSet( pieEntryList , "");

        pieDataSet.setSliceSpace(3f);
        pieDataSet.setSelectionShift(7f);
        pieDataSet.setColors(colorArray);
        pieDataSet.setHighlightEnabled(true);

//            pieDataSet.setDrawIcons(false);
//            pieDataSet.setIconsOffset(new MPPointF(0, 40));

        PieData pieData = new PieData(pieDataSet);
//            pieData.setValueFormatter(new PercentFormatter());
//            pieData.setValueTextColor(Color.WHITE);
            pieData.setValueTextSize(24f);
        pieData.setDrawValues(false);

        return pieData;

    }



    public void showAnimation(){

        pieChart.postDelayed(new Runnable() {
            @Override
            public void run() {

                pieChart.setVisibility(View.VISIBLE);
                pieChart.spin( 500, 0, -360f, Easing.EasingOption.EaseInOutQuad );
            }
        }, 1000);

    }

    @Override
    public void show() {
        super.show();
        showAnimation();
    }

    @Override
    public void hide() {
        super.hide();

    }

    private void finish(){

        dismiss();

        L.getInstance().log(this.getClass(),"[ Touch ] Touch");

        ((LoadingStartActivity) context).onBackPressed();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }
}
