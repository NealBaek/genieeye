package com.planb.genieeye.views.recognition;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.os.Environment;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by sonseongbin on 2017. 4. 3..
 */

public class CameraController implements View.OnTouchListener, Camera.AutoFocusCallback {

    private final String TAG = CameraController.class.getSimpleName();

    private Context context;
    private Camera camera;

    private Camera.ShutterCallback shutterCallback;
    private OnPictureTakenListener onPictureTakenListener;

    public CameraController(Context context) {
        this.context = context;
        init();
    }

    private void init() {
        camera = Camera.open();
        camera.setDisplayOrientation(90);
    }

    public void start(SurfaceView surfaceView) {
        try {
            camera.setPreviewDisplay(surfaceView.getHolder());
            camera.startPreview();
            surfaceView.setOnTouchListener(this);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stop() {
        camera.stopPreview();
        camera.release();
        camera = null;
    }

    public void setShutterCallback(Camera.ShutterCallback shutterCallback) {
        this.shutterCallback = shutterCallback;
    }

    public CameraParameter getCameraParameters() {

        Camera.Parameters parameters = camera.getParameters();

        List<Size> sizeList = new ArrayList<>();
        CameraParameter cameraParameter = new CameraParameter();

        for(Camera.Size size : parameters.getSupportedPreviewSizes()) {
            sizeList.add(new Size(size.width, size.height));
        }

        cameraParameter.setSupportedPreviewSizes(sizeList);
        sizeList = new ArrayList<>();

        for(Camera.Size size : parameters.getSupportedPictureSizes()) {
            sizeList.add(new Size(size.width, size.height));
        }

        cameraParameter.setSupportedPictureSizes(sizeList);
        sizeList = new ArrayList<>();

        for(Camera.Size size : parameters.getSupportedVideoSizes()) {
            sizeList.add(new Size(size.width, size.height));
        }

        cameraParameter.setSupportedVideoSizes(sizeList);

        return cameraParameter;
    }

    public void setPictureSize(Size size) {
        Camera.Parameters parameters = camera.getParameters();

        List<Camera.Size> sizeList = parameters.getSupportedPreviewSizes();

        if(sizeList == null || sizeList.get(0) == null){
            parameters.setPictureSize(size.width, size.height);
        }else{
            parameters.setPictureSize(sizeList.get(0).width, sizeList.get(0).height);
        }

        camera.setParameters(parameters);
    }




    public void takePicture() {
        camera.autoFocus(this);
    }


    public void setOnPictureTakenListener( OnPictureTakenListener onPictureTakenListener ){
        this.onPictureTakenListener = onPictureTakenListener;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                camera.autoFocus(new Camera.AutoFocusCallback() {
                    @Override
                    public void onAutoFocus(boolean success, Camera camera) {

                    }
                });
                break;
        }

        return true;
    }

    @Override
    public void onAutoFocus(boolean success, Camera camera) {
        Log.d(TAG, "onAutoFocus");
        camera.takePicture(shutterCallback, null, new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {

                try {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);

                    File photoFile = new File(Environment.getExternalStorageDirectory().getPath() + "/Download/"+
                            new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())+".jpg");

                    FileOutputStream outputStream = new FileOutputStream(photoFile);

                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);

                    outputStream.close();

                    onPictureTakenListener.onPictureTaken(photoFile);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    public interface OnPictureTakenListener{

        void onPictureTaken(File photoFile);

    }
}
