package com.planb.genieeye.views.home.fragments.watch;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by admin on 2018. 1. 10..
 */

@Setter @Getter @ToString @Builder
public class WatchModel {

    String watchName;
    String watchModelNumber;
    int watchImg;

}
