package com.planb.genieeye.views.home.fragments.watch;

import com.planb.genieeye.base.BaseFragmentContracts;


/**
 * Created by admin on 2017. 12. 13..
 */

public interface WatchContract extends BaseFragmentContracts {

    interface View extends BaseView {

        void startRecognitionActivity(android.view.View v);

    }

    interface Presenter extends BasePresenter {

        void setFragment(WatchFragment myAccountFragment);

    }

}
