package com.planb.genieeye.views.home.fragments.geniebox;

import android.app.Fragment;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.planb.genieeye.R;
import com.planb.genieeye.databinding.FragmentGenieboxBinding;
import java.util.ArrayList;

/**
 * Created by admin on 2018. 1. 9..
 */

public class GenieboxFragment extends Fragment{

    private FragmentGenieboxBinding genieboxBinding = null;
    private WebView webView = null;  // 2017.12.07


    public static GenieboxFragment createFor(String text) {
        GenieboxFragment fragment = new GenieboxFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        genieboxBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_geniebox, container, false);
        genieboxBinding.setGenieboxFragment(this);

        setUpWebView();

        return genieboxBinding.getRoot();
    }

    private void setUpWebView(){
        genieboxBinding.container.post(new Runnable() {
            @Override
            public void run() {
                webView = new CommonWebView(getActivity(), genieboxBinding.container);
                webView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));
                genieboxBinding.container.addView(webView);

                webView.loadUrl("http://geniebox.co.kr");
            }
        });
    }


}
