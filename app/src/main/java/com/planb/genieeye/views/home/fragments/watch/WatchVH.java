package com.planb.genieeye.views.home.fragments.watch;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.planb.genieeye.R;
import com.planb.genieeye.utils.L;
import com.planb.genieeye.utils.StringKeys;
import com.planb.genieeye.views.recognition.RecognitionActivity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * Created by admin on 2018. 1. 10..
 */

class WatchVH extends RecyclerView.ViewHolder {

    private ImageView image;
    private Context context;
    private boolean hasImageLoadedAleady = false;

    private int cPos;

    public WatchVH(View itemView) {
        super(itemView);

        image = itemView.findViewById(R.id.watch_img);

        context = itemView.getContext();

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, RecognitionActivity.class);
                intent.putExtra(StringKeys.WATCH_POSITION, getAdapterPosition());
                context.startActivity(intent);
            }
        });

    }

    public void setPosition( int cPos ){
        this.cPos = cPos;
    }

    public void bindCell( int watchImage ){


        if( !hasImageLoadedAleady ){

            Picasso.with(context)
                    .load( watchImage )
                    .resize(500,500)
                    .into(image, new Callback() {
                        @Override
                        public void onSuccess() {

                            itemView.post(new Runnable() {
                                @Override
                                public void run() {
                                    itemView.findViewById(R.id.gradient_view).setVisibility(View.VISIBLE);
                                }
                            });

                        }

                        @Override
                        public void onError() {

                        }
                    });

            hasImageLoadedAleady = true;
        }

    }
}