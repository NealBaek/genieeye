package com.planb.genieeye.views.home.fragments.watch;

/**
 * Created by admin on 2017. 12. 13..
 */

public class WatchPresenterImpl implements WatchContract.Presenter {

    private WatchFragment watchFragment = null;

    @Override
    public void setFragment(WatchFragment watchFragment) {
        this.watchFragment = watchFragment;
    }

    @Override
    public void init() {

        watchFragment.initAllViews();
    }


    @Override
    public void clearRef() {
        watchFragment = null;
    }

}
