package com.planb.genieeye.views.home.fragments.watch;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.planb.genieeye.R;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by admin on 2018. 1. 10..
 */

public class WatchAdapter extends RecyclerView.Adapter<WatchVH> {

    private ArrayList<WatchModel> data;

    public WatchAdapter(ArrayList<WatchModel> data) {
        this.data = data;
    }

    @Override
    public WatchVH onCreateViewHolder(ViewGroup parent, int viewType) {


        return
                new WeakReference<WatchVH>(
                        new WatchVH(
                                LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_watch, parent, false )
                        )
                ).get();

    }

    @Override
    public void onBindViewHolder(WatchVH holder, int cPos) {

        holder.setPosition(cPos);
        holder.bindCell( data.get(cPos).getWatchImg() );
    }

    @Override
    public int getItemCount() {
        return data.size();
    }




}
