package com.planb.genieeye.views.home;


import com.planb.genieeye.views.recognition.RecognitionActivity;

/**
 * Created by admin on 2017. 12. 13..
 */

public class HomePresenterImpl implements HomeContract.Presenter {

    private HomeActivity homeActivity = null;

    @Override
    public void setActivity(HomeActivity homeActivity) {
        this.homeActivity = homeActivity;
    }

    @Override
    public void init() {

        homeActivity.setBinding();
        homeActivity.initAllViews();

    }



    @Override
    public void clearRef() {
        homeActivity = null;
    }

}
