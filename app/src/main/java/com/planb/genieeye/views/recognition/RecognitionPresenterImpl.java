package com.planb.genieeye.views.recognition;


import android.Manifest;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.view.SurfaceHolder;
import android.widget.Toast;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.planb.genieeye.net.ResGenieEyeVo;
import com.planb.genieeye.net.RetrofitHelper;
import com.planb.genieeye.utils.ComparableUtils;
import com.planb.genieeye.utils.L;
import com.planb.genieeye.utils.StringUtils;

import java.io.File;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by admin on 2017. 12. 13..
 */

public class RecognitionPresenterImpl implements RecgonitionContract.Presenter, SurfaceHolder.Callback, Camera.ShutterCallback, CameraController.OnPictureTakenListener {

    private RecognitionActivity recognitionActivity = null;

    private SurfaceHolder surfaceHolder = null;
    private CameraController cameraController = null;

    @Override
    public void setActivity(RecognitionActivity loginActivity) {
        this.recognitionActivity = loginActivity;
    }

    @Override
    public void init() {

        recognitionActivity.setBinding();
        recognitionActivity.initAllViews();

    }


    @Override
    public void askCameraPermission() {
        TedPermission.with(recognitionActivity)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {

                        setUpCameraServiceView();
                        setUpCameraController();

                        cameraController.start(recognitionActivity.recognitionBinding.surfaceView);


                    }

                    @Override
                    public void onPermissionDenied(ArrayList<String> deniedPermissions) {

                    }
                })
                .setRationaleMessage("명품 확인을 위해 휴대폰 기능 사용 허가가 필요합니다.")
                .setDeniedMessage( "명품 확인을 안하시겠습니까?" )
                .setPermissions( Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE )
                .check();
    }

    private void setUpCameraServiceView(){

        surfaceHolder = recognitionActivity.recognitionBinding.surfaceView.getHolder();
        surfaceHolder.addCallback(this);


    }
    private void setUpCameraController(){
        cameraController = new CameraController(recognitionActivity);
        cameraController.setPictureSize(new Size(1280,720));
        cameraController.setShutterCallback(this);
        cameraController.setOnPictureTakenListener(this);

    }




    @Override
    public void surfaceCreated(SurfaceHolder holder) {

        recognitionActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setUpCameraController();
                cameraController.start(recognitionActivity.recognitionBinding.surfaceView);
            }
        });
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

        recognitionActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                cameraController.stop();
            }
        });

    }


    @Override
    public void takePicture(){

        if( cameraController != null ){
            cameraController.takePicture();
        }

    }

    @Override
    public void onShutter() {

    }

    @Override
    public void onPictureTaken(File photoFile) {

        recognitionActivity.showProgress("사진 인식 중");

        L.getInstance().log(this.getClass(),"[ PictureTaken ] enter");


        RequestBody requestFile =
                RequestBody.create(
//                        MediaType.parse( "multipart/form-data" ),
                        MediaType.parse( "image/*" ),
                        photoFile
                );

        MultipartBody.Part photoMultipart = MultipartBody.Part.createFormData( "img", photoFile.getName(), requestFile );




        RetrofitHelper.getInstance().getProfileAPI().callGenieEyeAPI(photoMultipart).enqueue(new Callback<ResGenieEyeVo>() {
            @Override
            public void onResponse(Call<ResGenieEyeVo> call, Response<ResGenieEyeVo> response) {

                L.getInstance().log(this.getClass(),"[ onPictureTaken ] response Code : " + response.code());

                ResGenieEyeVo resGenieEyeVo = response.body();

                if(resGenieEyeVo == null){
                    L.getInstance().log(this.getClass(),"[ onPictureTaken ] response Error : " + response.errorBody().toString());
                    recognitionActivity.dismissProgress();
                    Toast.makeText(recognitionActivity, "모듈 서버 응답 없음", Toast.LENGTH_SHORT).show();
                    return;
                }

                L.getInstance().log(this.getClass(),"[ onPictureTaken ] response Body : " + resGenieEyeVo);


                // prob => [ 롤렉스 데이트저스트, 롤렉스 익스플로러, 오메가 씨마스터(블랙, 청판, 블랙 모조품), 이외 다른 물품 ]
                // (prob=["0.9787", "0.0000", "0.0002", "0.0207", "0.0001", "0.0003"]

                try{

                    String[] probArray = StringUtils.parseToArray( resGenieEyeVo.getResult().getProb() );

                    if( Float.parseFloat( probArray[probArray.length - 1] ) > 0.6f ){

                        // 시계가 아님
                        recognitionActivity.dismissProgress();
                        Toast.makeText(recognitionActivity, "명품이 좀더 잘나오게 찍어주세요!", Toast.LENGTH_SHORT).show();

                        return;
                    }


                    float prob = 0;

                    switch ( recognitionActivity.WATCH_POSITION ){

                        case 0 :  // 롤렉스 데이트저스트

                            prob = Float.parseFloat( probArray[0] );
                            break;

                        case 1 :  // 롤렉스 익스플로러

                            prob = Float.parseFloat( probArray[1] );
                            break;

                        case 2 :  // 오메가 씨마스터(블랙, 청판, 블랙 모조품)

                            prob = ComparableUtils.selectBiggest(
                                    Float.parseFloat( probArray[2] ),
                                    Float.parseFloat( probArray[3] ),
                                    Float.parseFloat( probArray[4] )
                            );
                            break;
                    }

                    L.getInstance().log(this.getClass(),"[ onPictureTaken ] prob : " + prob + " // watch_pos : " + recognitionActivity.WATCH_POSITION);


                    recognitionActivity.startLoadingActivity( photoFile.getAbsolutePath(), prob);

                }catch (Exception e){

                    recognitionActivity.dismissProgress();
                    Toast.makeText(recognitionActivity, "Error : " + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<ResGenieEyeVo> call, Throwable t) {
                L.getInstance().log(this.getClass(),"[ onPictureTaken ] response Error : " + t.getMessage());
                recognitionActivity.dismissProgress();
                Toast.makeText(recognitionActivity, "모듈 서버 응답 없음 Error : " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public void clearRef() {
        recognitionActivity = null;
    }

}
