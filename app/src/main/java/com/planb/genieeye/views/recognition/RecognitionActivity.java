package com.planb.genieeye.views.recognition;

import android.app.ActivityOptions;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.transition.Explode;
import android.transition.Fade;
import android.transition.Slide;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.planb.genieeye.LoadingStartActivity;
import com.planb.genieeye.R;
import com.planb.genieeye.base.BaseActivity;
import com.planb.genieeye.databinding.ActivityRecognitionBinding;
import com.planb.genieeye.utils.L;
import com.planb.genieeye.utils.StringKeys;
import com.planb.genieeye.views.dialog.LoadingDialog;

public class RecognitionActivity extends BaseActivity implements RecgonitionContract.View {

    public ActivityRecognitionBinding recognitionBinding = null;
    private RecognitionPresenterImpl recognitionPresenter = null;

    public int WATCH_POSITION;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        super.onCreate(savedInstanceState);

        WATCH_POSITION = getIntent().getIntExtra(StringKeys.WATCH_POSITION, 0);

        L.getInstance().log(this.getClass(), "[ 가즈아 ] WATCH_POSITION : "+ WATCH_POSITION);

        recognitionPresenter = new RecognitionPresenterImpl();
        recognitionPresenter.setActivity(this);
        recognitionPresenter.init();
    }

    @Override
    public void setBinding() {
        recognitionBinding = DataBindingUtil.setContentView(this, R.layout.activity_recognition);
        recognitionBinding.setRecognitionActivity(this);
    }

    @Override
    public void initAllViews() {

        setUpWatchInfo();
        recognitionPresenter.askCameraPermission();


    }



    private void setUpWatchInfo(){

        String watchName = StringKeys.nameArray[WATCH_POSITION];
        String watchNum  = StringKeys.numberArray[WATCH_POSITION];

        recognitionBinding.watchNameTv.setText( watchName );
        recognitionBinding.watchProductNumberTv.setText( watchNum );

    }


    @Override
    public void startRecognition(View v) {

        recognitionPresenter.takePicture();
    }

    @Override
    public void startLoadingActivity(String filePath, float prob){

        recognitionBinding.watchBtnLayout.setEnabled(false);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(RecognitionActivity.this, LoadingStartActivity.class);
//                Bundle bundle = ActivityOptions.makeSceneTransitionAnimation(RecognitionActivity.this).toBundle();

//                intent.setAction("com.planb.genieeye.watchimg");


                intent.putExtra(StringKeys.WATCH_IMG_PATH, filePath);
                intent.putExtra(StringKeys.WATCH_PROB, prob);
                intent.putExtra(StringKeys.WATCH_POSITION, WATCH_POSITION);


                startActivity(intent);
                finish();

            }
        });

    }


    @Override
    public void onBack(View v) {
        finish();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }
}