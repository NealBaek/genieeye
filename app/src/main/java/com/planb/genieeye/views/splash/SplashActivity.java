package com.planb.genieeye.views.splash;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.planb.genieeye.R;
import com.planb.genieeye.base.BaseActivity;
import com.planb.genieeye.databinding.ActivitySplashBinding;
import com.planb.genieeye.views.home.HomeActivity;

public class SplashActivity extends BaseActivity implements SplashContract.View {

    private ActivitySplashBinding splashBinding = null;
    private SplashPresenterImpl splashPresenter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        splashPresenter = new SplashPresenterImpl();
        splashPresenter.setActivity(this);
        splashPresenter.init();
    }

    @Override
    public void setBinding() {

        splashBinding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        splashBinding.setSplashActivity(this);

    }

    @Override
    public void initAllViews() {

    }

    @Override
    public void startHomeActivity() {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();
    }
}
