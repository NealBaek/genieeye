package com.planb.genieeye.views.home.fragments.geniebox;

/**
 * Created by admin on 2017. 11. 20..
 */

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.planb.genieeye.base.BaseActivity;
import com.planb.genieeye.utils.PaymentScheme;

import java.net.URISyntaxException;



public class BaseWebViewClient extends WebViewClient {

    public Context cont;
    public BaseActivity activity;
    public WebView webView;
    public static final String TAG = "KakaoSamplePageTestApp";
    public static final String KAKAOTALK_PACKAGE_NAME = "com.kakao.talk";
    public static final String KAKAOTALK_CUSTOM_URL = "kakaotalk:";
    public static final String GOOGLE_PLAY_STORE_PREFIX = "market://details?id=";
    public static final String INTENT_PROTOCOL_START = "intent:";
    public static final String PACKAGE_NAME_START = "package=";
    public static final String INTENT_PROTOCOL_END = ";end";
    private StringBuilder sb;
    private String urls;


    public BaseWebViewClient(BaseActivity activity, WebView webView){
        this.cont = activity;
        this.activity = activity;
        this.webView = webView;

    }
    /**
     * url 주소에 해당하는 웹페이지를 로딩
     */
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        //Log.d(Constants.TAG, "shouldOverrideUrlLoading url:" + url);

        if (!url.startsWith("http://") && !url.startsWith("https://") && !url.startsWith("javascript:")) {
            Intent intent = null;

            try {
                intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME); //IntentURI처리
                Uri uri = Uri.parse(intent.getDataString());

                activity.startActivity(new Intent(Intent.ACTION_VIEW, uri)); //해당되는 Activity 실행
                return true;
            } catch (URISyntaxException ex) {
                return false;
            } catch (ActivityNotFoundException e) {
                if ( intent == null )   return false;

                if ( handleNotFoundPaymentScheme(intent.getScheme()) )  return true; //설치되지 않은 앱에 대해 사전 처리(Google Play이동 등 필요한 처리)

                String packageName = intent.getPackage();
                if (packageName != null) { //packageName이 있는 경우에는 Google Play에서 검색을 기본
                    activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)));
                    return true;
                }

                return false;
            }
        }else if(url.startsWith("https://open.kakao.com")){
            if(url.startsWith("https://open.kakao.com")) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                Uri u = Uri.parse(url);
                i.setData(u);
                activity.startActivity(i);
                activity.onBackPressed();
                return true;
            }
        }
        return false;
    }

    public void goToPlayStore(String url) {
        Intent intent = null;
        if (url.startsWith(KAKAOTALK_CUSTOM_URL)) {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(GOOGLE_PLAY_STORE_PREFIX + KAKAOTALK_PACKAGE_NAME));
        } else if (url.startsWith(INTENT_PROTOCOL_START)) {
            final int packageStartIndex = url.indexOf(PACKAGE_NAME_START) + PACKAGE_NAME_START.length();
            final int packageEndIndex = url.indexOf(INTENT_PROTOCOL_END);
            //Log.d(TAG, "packageStartIndex : " + packageStartIndex + ", packageEndIndex : " + packageEndIndex);
            final String packageName = url.substring(packageStartIndex, packageEndIndex < 0 ? url.length()
                    : packageEndIndex);
            //Log.d(TAG, "packageName : " + packageName);
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(GOOGLE_PLAY_STORE_PREFIX + packageName));
        }
        cont.startActivity(intent);
    }


    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        //Log.d(Constants.TAG, "onPageStarted not url:" + url);
    }

    @Override
    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

    }

    @Override
    public void onPageFinished(WebView view, String url) {
        //Log.d(Constants.TAG ,"onPageFinished url:" + url);
        //view.loadUrl("javascript:put_ios_token('"+UserData.newInstance(activity).getPush_token()+"')");

        super.onPageFinished(view, url);
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP){
            CookieSyncManager.getInstance().sync();
        }else {
            CookieManager.getInstance().flush();
        }


        //webView.loadUrl("javascript:getToken('" + UserData.newInstance(cont).getPush_token()+"')");
		/*webView.loadUrl(
				"javascript:"+ "$(\"#token\").val('"+ UserData.newInstance(cont).getPush_token()+"');"
		);*/

		/*activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Log.d(Constants.TAG, "javascript:getToken");
				webView.loadUrl("javascript:getToken('"+UserData.newInstance(cont).getPush_token()+"')");
			}
		});*/

    }

    /**
     * @param scheme
     * @return 해당 scheme에 대해 처리를 직접 하는지 여부
     *
     * 결제를 위한 3rd-party 앱이 아직 설치되어있지 않아 ActivityNotFoundException이 발생하는 경우 처리합니다.
     * 여기서 handler되지않은 scheme에 대해서는 intent로부터 Package정보 추출이 가능하다면 다음에서 packageName으로 market이동합니다.
     *
     */
    protected boolean handleNotFoundPaymentScheme(String scheme) {
        //PG사에서 호출하는 url에 package정보가 없어 ActivityNotFoundException이 난 후 market 실행이 안되는 경우
        if ( PaymentScheme.ISP.equalsIgnoreCase(scheme) ) {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + PaymentScheme.PACKAGE_ISP)));
            return true;
        } else if ( PaymentScheme.BANKPAY.equalsIgnoreCase(scheme) ) {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + PaymentScheme.PACKAGE_BANKPAY)));
            return true;
        }

        return false;
    }
}

