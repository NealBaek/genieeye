package com.planb.genieeye.views.home;


/**
 * Created by admin on 2017. 12. 13..
 */

import android.view.View;

import com.planb.genieeye.base.BaseContracts;
import com.planb.genieeye.views.recognition.RecognitionActivity;

public interface HomeContract extends BaseContracts {


    interface View extends BaseView{

    }

    interface Presenter extends BasePresenter{


        void setActivity(HomeActivity homeActivity);
    }


}
