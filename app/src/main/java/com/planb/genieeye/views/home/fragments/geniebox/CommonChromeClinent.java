package com.planb.genieeye.views.home.fragments.geniebox;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ViewGroup;
import android.webkit.ConsoleMessage;
import android.webkit.GeolocationPermissions;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

public class CommonChromeClinent extends WebChromeClient {

	private Context context;
	private AppCompatActivity activity;
	private String[] mimetypes = {"image/*"};
	private FrameLayout fl_container;

	public CommonChromeClinent(AppCompatActivity activity, FrameLayout fl_container){
		this.context   = activity;
		this.activity  = activity;
		this.fl_container   = fl_container;
	}

	@Override
    public boolean onJsAlert(WebView view, String url, final String message, final android.webkit.JsResult result)
    {
		new AlertDialog.Builder(context)
				.setMessage(message)
				.setPositiveButton(android.R.string.ok,
						new AlertDialog.OnClickListener()
						{
							public void onClick(DialogInterface dialog, int which)
							{
								result.confirm();
							}
						})
				.setCancelable(false)
				.create()
				.show();


 
        return true;
    };

    @Override
    public void onProgressChanged(WebView view, int newProgress) {
    	super.onProgressChanged(view, newProgress);
    }

	@Override
	public boolean onCreateWindow(WebView view, boolean dialog, boolean userGesture, Message resultMsg) {

		/** 웹뷰 생성자내부 init에서 웹뷰 클라이언트 설정 추가  */
		WebView new_webview = new CommonWebView(context, fl_container);

		new_webview.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));

		new_webview.setWebViewClient(new CommonWebViewClient(activity, new_webview));
		WebView.WebViewTransport transport = (WebView.WebViewTransport)resultMsg.obj;
		transport.setWebView(new_webview);
		resultMsg.sendToTarget();

		fl_container.addView(new_webview);

		return true;
	}

	@Override
	public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
		// Should implement this function.
		final String myOrigin = origin;
		final GeolocationPermissions.Callback myCallback = callback;
		/*AlertDialog.Builder builder = new AlertDialog.Builder(activity);

		builder.setTitle("위치확인동의");
		builder.setMessage("위치확인에 동의해주셔야 모든 기능을 사용하실 수 있습니다");
		builder.setPositiveButton("동의", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				myCallback.invoke(myOrigin, true, false);
			}
		});

		builder.setNegativeButton("취소", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				myCallback.invoke(myOrigin, false, false);
			}
		});

		//Log.d("seum","onGeolocationPermissionsShowPrompt");

		AlertDialog alert = builder.create();*/

		myCallback.invoke(myOrigin, true, false);
	}

		@Override
	public void onCloseWindow(WebView window) {
			super.onCloseWindow(window);
		//Log.d(Constants.TAG, "count onCloseWindow");
		fl_container.removeView(window);
	}





}

