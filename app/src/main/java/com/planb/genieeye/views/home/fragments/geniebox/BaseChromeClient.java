package com.planb.genieeye.views.home.fragments.geniebox;

/**
 * Created by admin on 2017. 11. 20..
 */

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;
import android.view.ViewGroup;
import android.webkit.ConsoleMessage;
import android.webkit.GeolocationPermissions;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.planb.genieeye.base.BaseActivity;



public class BaseChromeClient extends WebChromeClient {

    private Context context;
    private BaseActivity activity;
    private String[] mimetypes = {"image/*"};
    private FrameLayout fl_container;

    public BaseChromeClient(BaseActivity activity, FrameLayout fl_container){
        this.context   = activity;
        this.activity  = activity;
        this.fl_container   = fl_container;
    }

    @Override
    public boolean onJsAlert(WebView view, String url, final String message, final android.webkit.JsResult result)
    {
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok,
                        new AlertDialog.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int which)
                            {
                                result.confirm();
                            }
                        })
                .setCancelable(false)
                .create()
                .show();



        return true;
    };

    @Override
    public void onProgressChanged(WebView view, int newProgress) {
        super.onProgressChanged(view, newProgress);
    }

    @Override
    public boolean onCreateWindow(WebView view, boolean dialog, boolean userGesture, Message resultMsg) {
        //Log.d(Constants.TAG, "onCreateWindow");
        WebView new_webview = new CommonWebView(context, fl_container);
        new_webview.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));
        fl_container.addView(new_webview);
        WebView.WebViewTransport transport = (WebView.WebViewTransport)resultMsg.obj;
        transport.setWebView(new_webview);
        resultMsg.sendToTarget();

        return true;
    }


    @Override
    public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
        // Should implement this function.
        final String myOrigin = origin;
        final GeolocationPermissions.Callback myCallback = callback;

        myCallback.invoke(myOrigin, true, false);
    }

    @Override
    public void onCloseWindow(WebView window) {
        super.onCloseWindow(window);
        //Log.d(Constants.TAG, "count onCloseWindow");
        fl_container.removeView(window);
    }
}
