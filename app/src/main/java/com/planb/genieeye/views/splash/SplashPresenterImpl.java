package com.planb.genieeye.views.splash;


import android.os.Handler;
import android.os.Message;

import com.planb.genieeye.views.recognition.RecgonitionContract;
import com.planb.genieeye.views.recognition.RecognitionActivity;

/**
 * Created by admin on 2017. 12. 13..
 */

public class SplashPresenterImpl implements SplashContract.Presenter {

    private SplashActivity splashActivity = null;

    @Override
    public void setActivity(SplashActivity splashActivity) {
        this.splashActivity = splashActivity;
    }

    @Override
    public void init() {

        splashActivity.setBinding();
        splashActivity.initAllViews();

        handler.sendEmptyMessageDelayed(0, 1000);

    }


    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            splashActivity.startHomeActivity();

        }
    };



    @Override
    public void clearRef() {
        splashActivity = null;
    }

}
