package com.planb.genieeye.views.home;

import android.app.Fragment;
import android.content.res.TypedArray;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.planb.genieeye.R;
import com.planb.genieeye.base.BaseActivity;
import com.planb.genieeye.databinding.ActivityHomeBinding;
import com.planb.genieeye.views.home.fragments.geniebox.GenieboxFragment;
import com.planb.genieeye.views.home.fragments.watch.WatchFragment;
import com.planb.genieeye.views.home.menu.DrawerAdapter;
import com.planb.genieeye.views.home.menu.DrawerItem;
import com.planb.genieeye.views.home.menu.SimpleItem;
import com.yarolegovich.slidingrootnav.SlidingRootNav;
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder;

import java.util.Arrays;

public class HomeActivity extends BaseActivity implements HomeContract.View, DrawerAdapter.OnItemSelectedListener {

    private ActivityHomeBinding homeBinding = null;
    private HomePresenterImpl homePresenter = null;

    private SlidingRootNav slidingRootNav;
    private Bundle savedInstanceState = null;



    private static final int POS_HOME = 0;
    private static final int POS_GENIEBOX = 1;

    private String[] screenTitles;
    private Drawable[] screenIcons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.savedInstanceState = savedInstanceState;

        homePresenter = new HomePresenterImpl();
        homePresenter.setActivity(this);
        homePresenter.init();

    }


    @Override
    public void setBinding() {
        homeBinding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        homeBinding.setHomeActivity(this);
    }

    @Override
    public void initAllViews() {

        setUpFragments();
        setUpNav(savedInstanceState);

    }



    private void setUpNav(Bundle savedInstanceState){


        setSupportActionBar(homeBinding.toolbar);

        slidingRootNav = new SlidingRootNavBuilder(this)
                .withToolbarMenuToggle(homeBinding.toolbar)
                .withMenuOpened(false)
                .withContentClickableWhenMenuOpened(false)
                .withSavedState(savedInstanceState)
                .withMenuLayout(R.layout.menu_left_drawer)
                .inject();

        screenIcons = loadScreenIcons();
        screenTitles = loadScreenTitles();

        DrawerAdapter adapter = new DrawerAdapter(Arrays.asList(
                createItemFor(POS_HOME).setChecked(true),
                createItemFor(POS_GENIEBOX)));

        adapter.setListener(this);

        RecyclerView list = findViewById(R.id.list);
        list.setNestedScrollingEnabled(false);
        list.setLayoutManager(new LinearLayoutManager(this));
        list.setAdapter(adapter);

        adapter.setSelected(POS_HOME);

    }
    private void setUpFragments(){

        if(watchFragment == null) {
            watchFragment = WatchFragment.createFor("");
        }
        if(genieboxFragment == null){
            genieboxFragment = GenieboxFragment.createFor("");
        }
    }

    private GenieboxFragment genieboxFragment = null;
    private WatchFragment watchFragment = null;

    @Override
    public void onItemSelected(int position) {

        slidingRootNav.closeMenu();

        switch (position){
            case 0:

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        showFragment(watchFragment);
                    }
                });

                break;
            case 1:

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        showFragment(genieboxFragment);
                    }
                });

                break;
        }



    }

    private void showFragment(Fragment fragment) {
        getFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }

    private DrawerItem createItemFor(int position) {
        return new SimpleItem(screenIcons[position], screenTitles[position])
                .withIconTint(color(R.color.pureWhiteAlpha))
                .withTextTint(color(R.color.pureWhiteAlpha))
                .withSelectedIconTint(color(R.color.pureWhite))
                .withSelectedTextTint(color(R.color.pureWhite));
    }

    private String[] loadScreenTitles() {
        return getResources().getStringArray(R.array.ld_activityScreenTitles);
    }

    private Drawable[] loadScreenIcons() {
        TypedArray ta = getResources().obtainTypedArray(R.array.ld_activityScreenIcons);
        Drawable[] icons = new Drawable[ta.length()];
        for (int i = 0; i < ta.length(); i++) {
            int id = ta.getResourceId(i, 0);
            if (id != 0) {
                icons[i] = ContextCompat.getDrawable(this, id);
            }
        }
        ta.recycle();
        return icons;
    }

    @ColorInt
    private int color(@ColorRes int res) {
        return ContextCompat.getColor(this, res);
    }




    @Override
    protected void onDestroy() {
        super.onDestroy();

        savedInstanceState = null;
    }
}
