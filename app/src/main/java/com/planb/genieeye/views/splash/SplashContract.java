package com.planb.genieeye.views.splash;


/**
 * Created by admin on 2017. 12. 13..
 */

import com.planb.genieeye.base.BaseContracts;

public interface SplashContract extends BaseContracts {


    interface View extends BaseView{

        void startHomeActivity();

    }

    interface Presenter extends BasePresenter{


        void setActivity(SplashActivity splashActivity);
    }


}
