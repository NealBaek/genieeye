package com.planb.genieeye.views.home.fragments.geniebox;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.FrameLayout;

import com.planb.genieeye.base.BaseActivity;



public class CommonWebView extends WebView {

	public CommonWebView(Context context, FrameLayout fl_container) {
		super(context);
		init((BaseActivity)context, fl_container);
	}

	public CommonWebView(Context context, AttributeSet attrs, FrameLayout fl_container) {
		super(context, attrs);
		init((BaseActivity)context, fl_container);
	}
	
	public CommonWebView(Context context, AttributeSet attrs, int defStyle, FrameLayout fl_container) {
		super(context, attrs, defStyle);
		init((BaseActivity)context, fl_container);
	}

	public void init(BaseActivity activity, FrameLayout fl_container){


		this.getSettings().setJavaScriptEnabled(true);
		this.getSettings().setAllowFileAccess(true);
		this.getSettings().setAllowContentAccess(true);
		this.getSettings().setDomStorageEnabled(true);
		this.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
		this.getSettings().setSupportMultipleWindows(false);
		this.getSettings().setUseWideViewPort(true);
		this.getSettings().setDatabaseEnabled(true);
		this.getSettings().setAppCacheEnabled(true);
		this.getSettings().setGeolocationEnabled(true);
		this.getSettings().setPluginState(WebSettings.PluginState.ON);
//		this.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK); // static url을 로딩하는 경우라면 LOAD_CACHE_ELSE_NETWORK 를 사용하여 로딩속도를 대폭 샹상 시킬 수 있다
		this.getSettings().setSavePassword(false);

//		this.getSettings().setUserAgentString("Mozilla / 5.0 (Windows NT 6.1; WOW64; rv : 46.0) Gecko / 20100101 Firefox / 46.0");



		/**  쿠키 설정 */

		CookieManager cookieManager = CookieManager.getInstance();
		cookieManager.setAcceptCookie(true);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			this.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
			cookieManager.setAcceptThirdPartyCookies(this, true);
		}




		/** 웹뷰 클라이언트 설정 */
		this.setWebViewClient(new CommonWebViewClient(activity, this));
		this.setWebChromeClient(new CommonChromeClinent(activity, fl_container));

        this.getSettings().setUserAgentString(this.getSettings().getUserAgentString() + " JANDROID");
	}


}
