package com.planb.genieeye.views.recognition;


/**
 * Created by admin on 2017. 12. 13..
 */

import android.graphics.Bitmap;
import android.view.View;

import com.planb.genieeye.base.BaseContracts;

public interface RecgonitionContract extends BaseContracts {


    interface View extends BaseView{

        void startRecognition(android.view.View v);
        void onBack(android.view.View v);
        void startLoadingActivity(String filePath, float result);

    }

    interface Presenter extends BasePresenter{

        void setActivity(RecognitionActivity recognitionActivity);
        void askCameraPermission();

        void takePicture();

    }


}
