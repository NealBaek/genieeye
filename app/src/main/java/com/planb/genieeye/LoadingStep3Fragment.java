package com.planb.genieeye;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.planb.genieeye.base.BaseFragmentContracts;
import com.planb.genieeye.databinding.FragmentLoadingStep1Binding;
import com.planb.genieeye.databinding.FragmentLoadingStep3Binding;

public class LoadingStep3Fragment extends Fragment implements BaseFragmentContracts.BaseView {

    private FragmentLoadingStep3Binding step3Binding = null;

    public LoadingStep3Fragment() {
        // Required empty public constructor
    }


    public static LoadingStep3Fragment newInstance() {
        LoadingStep3Fragment fragment = new LoadingStep3Fragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
//        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        step3Binding = DataBindingUtil.inflate(inflater, R.layout.fragment_loading_step3, container, false);
        step3Binding.setStep3Fragment(this);

        return step3Binding.getRoot();
    }

    @Override
    public void initAllViews() {

    }
}
