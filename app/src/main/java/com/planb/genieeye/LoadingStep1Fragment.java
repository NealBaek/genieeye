package com.planb.genieeye;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.planb.genieeye.base.BaseFragmentContracts;
import com.planb.genieeye.databinding.FragmentLoadingStep1Binding;

public class LoadingStep1Fragment extends Fragment implements BaseFragmentContracts.BaseView{

    private FragmentLoadingStep1Binding step1Binding = null;

    public LoadingStep1Fragment() {
        // Required empty public constructor
    }


    public static LoadingStep1Fragment newInstance() {
        LoadingStep1Fragment fragment = new LoadingStep1Fragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
//        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        step1Binding = DataBindingUtil.inflate(inflater, R.layout.fragment_loading_step1, container, false);
        step1Binding.setStep1Fragment(this);



        return step1Binding.getRoot();
    }

    @Override
    public void initAllViews() {

    }
}
