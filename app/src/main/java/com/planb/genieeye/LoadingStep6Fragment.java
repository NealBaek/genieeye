package com.planb.genieeye;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.planb.genieeye.base.BaseFragmentContracts;
import com.planb.genieeye.databinding.FragmentLoadingStep5Binding;
import com.planb.genieeye.databinding.FragmentLoadingStep6Binding;
import com.planb.genieeye.utils.L;
import com.planb.genieeye.utils.StringKeys;
import com.planb.genieeye.views.dialog.LoadingDialog;

import java.util.ArrayList;

public class LoadingStep6Fragment extends Fragment implements BaseFragmentContracts.BaseView {

    private FragmentLoadingStep6Binding step6Binding = null;
    private float probability = 0;

    private LoadingDialog loadingDialog = null;

    public LoadingStep6Fragment() {
        // Required empty public constructor
    }


    public static LoadingStep6Fragment newInstance(float result) {
        LoadingStep6Fragment fragment = new LoadingStep6Fragment();
        Bundle args = new Bundle();

        args.putFloat(StringKeys.WATCH_PROB, result);

        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            probability = getArguments().getFloat(StringKeys.WATCH_PROB, 0);

            L.getInstance().log(this.getClass(),"[ onPictureTaken ] probability : " + probability);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        step6Binding = DataBindingUtil.inflate(inflater, R.layout.fragment_loading_step6, container, false);
        step6Binding.setStep6Fragment(this);


        initAllViews();


        return step6Binding.getRoot();

    }

    @Override
    public void initAllViews() {

        loadingDialog = new LoadingDialog( getContext(), probability );

    }

    public void showPopUp(){

        loadingDialog.show();

    }


}
